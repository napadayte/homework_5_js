### Теоретичні питання

1. Як можна створити функцію та як ми можемо її викликати?

   У JavaScript функцію можна створити кількома способами:

   - Оголошення функції (Function Declaration):
     
     function greet() {
         console.log("Hello, World!");
     }
     // Виклик функції
     greet();
     

   - Функціональний вираз (Function Expression):
     
     let greet = function() {
         console.log("Hello, World!");
     };
     // Виклик функції
     greet();
     

   - Стрілочна функція (Arrow Function):
     
     let greet = () => {
         console.log("Hello, World!");
     };
     // Виклик функції
     greet();
     

2. Що таке оператор `return` в JavaScript? Як його використовувати в функціях?

   Оператор `return` в JavaScript використовується для повернення значення з функції і завершення її виконання. Якщо `return` використовується без значення, функція повертає `undefined`.

   
   function sum(a, b) {
       return a + b;
   }
   let result = sum(3, 4); // result буде дорівнювати 7
   

3. Що таке параметри та аргументи в функціях та в яких випадках вони використовуються?

   - Параметри — це змінні, які визначаються в оголошенні функції і слугують для прийому значень при виклику функції.
     
     function greet(name) {
         console.log("Hello, " + name + "!");
     }
     
   - Аргументи — це значення, які передаються функції при її виклику.
     
     greet("Bohdan"); // "Hello, Bohdan!"
     

   Параметри використовуються для отримання вхідних даних, які необхідні для виконання коду функції, а аргументи — це конкретні значення, що передаються функції при її виклику.

4. Як передати функцію аргументом в іншу функцію?

   У JavaScript функції можуть передаватися як аргументи іншим функціям. Це дозволяє створювати функції вищого порядку.

   
   function greet(name) {
       console.log("Hello, " + name + "!");
   }

   function processUserInput(callback) {
       let name = prompt("Please enter your name:");
       callback(name);
   }

   // Передаємо функцію greet як аргумент функції processUserInput
   processUserInput(greet);
   

   В цьому прикладі функція `greet` передається як аргумент функції `processUserInput`. Коли `processUserInput` викликається, вона отримує `greet` як параметр `callback` і викликає її з переданим аргументом `name`.